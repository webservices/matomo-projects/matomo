<?php

/**
 * Part of Piwik Login Shibboleth Plug-in.
 */

namespace Piwik\Plugins\LoginCERN;

use Piwik\Piwik;
use Exception;

/**
 * The MIT License (MIT)
 * Copyright © 2014-2016 Pouyan Azari, http://go.uniwue.de/azari <pouyan.azari@uni-wuerburg.de>
 * Copyright © 2014-2016 University of Wuerzburg, http://go.uniwue.de/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * API functionalities for settings.
 *
 * API is used by the Setting to write or read settings from the configuration file.
 * If this plug-in should have any other API related functions, they should be added here.
 * The API is only available to the user with SuperUser access.
 *
 */
class API extends \Piwik\Plugin\API
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Saves the Login Shibboleth settings in the config file automatically.
     *
     * @param string $data JSON encoded config array.
     *
     * @return array
     *
     * @throws Exception if user does not have super access, if this is not a POST method or
     *                   if JSON is not supplied.
     */
    public function saveConfig($data)
    {
        $this->checkHttpMethodIsPost();
        Piwik::checkUserHasSuperUserAccess();
        Config::savePluginOptions($data);
        return array('result' => 'success', 'message' => Piwik::translate('General_YourChangesHaveBeenSaved'));
    }

    /**
     * Check is the method sending the data is post.
     *
     * @throws \Exception
     */
    private function checkHttpMethodIsPost()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception('Invalid HTTP method.');
        }
    }
}
