# Matomo Web Analytics

### Overview

This repository includes a Dockerfile, a gitlab-ci file, entry point scripts, configuration files, environment files, a custom login plugin for Matomo and a changed groups plugin to leverage egroup permissions.

The application is structured as follows. Three containers reside in a common pod, the first one running Matomo on top of an Apache
Web Server, the second one running a Shibboleth daemon for Single Sign-On authentication and the third one running Logstash for Matomo Apache Central Logging. Matomo is reachable outside the OpenShift cluster via the URL **https://NAMESPACE.web.cern.ch**. The resources reachable by this base URL are protected by Shibboleth.

##### Where to find what
- The *Dockerfile* contains the shared image blueprint for the Matomo and Shibboleth containers
- *conf_files/apache.sh* contains the entry point script for the Matomo container
- *conf_files/shibboleth.sh* contains the entry point script for the Shibboleth container
- *conf_files/config.ini.php* is the Matomo configuration file
- *conf_files/httpd.conf* is the Apache configuration file
- *conf_files/shib_httpd.conf* is the Shibboleth configuration file for Apache
- *conf_files/matomo_httpd.conf* contains the Apache virtual hosts configuration
- *conf_files/liveness.py* contains the health checks for the Matomo container
- *conf_files/shibboleth2.xml* contains the adapted CERN Shibboleth configuration
- The custom login plugin for matomo can be found in *LoginCERN/*
- The groups plugin for matomo can be found in *GroupPermissions/*
- The Helm chart can be found in *helm/*, which contains all the required Openshift resources.

### The Helm Chart contains:

* A cronjob used for the archiving process (be sure to have browser archiving off)
* A cronjob used for the egroup sychronization
* An imagestream, a secret that contains the db credentials and the superuser token, two routes that connect to the two different services (one with sso, one unprotected), the two servives and the deploymentconfig

### LoginCERN - Custom Login Plugin for Matomo
This plugin has been developed for Single-Sign On/Authentication for Matomo. There are two authentication use cases covered by this plugin:

#### 1. Shibboleth Authentication for Website Requests
**Important: The LoginCERN plugin does not implement the Shibboleth authentication routines itself! Therefore, using the plugin without access to Matomo being restricted by a seperate Shibboleth deamon and the proper Apache configuration will potentially grant full access to all Matomo resources to any user!**

In case the Matomo virtual host is secured by Shibboleth, the plugin uses the Shibboleth authentication information in the PHP *\$_SERVER* array. If a user requests any Matomo resource, the plugin checks if the user given the information in *\$_SERVER* already exists in Matomo's database. If so, it the access is simply granted according to the existing user's access rights. If the user does not exist in the database, a new user is created and no access rights are granted at all.

As of now, the plugin is configurable by the following parameters. These can either be set in the *config.ini.php* file or directly via Matomo's web interface (for super users only):

- **logout_url**: Matomo redirects to this url after logging out the user
- **shibboleth_user_login** = "login": The value in *$_SERVER\[login\]* represents the Matomo user name
- **shibboleth_user_alias** = "alias": The value in *$_SERVER\[alias\]* represents the Matomo user alias
- **shibboleth_user_email** = "mail": The value in *$_SERVER\[mail\]* represents the Matomo user's email
- **shibboleth_group** = "group": The value(s) in *$_SERVER\[group\]* represent(s) the users group memberships (e.g. it-dep, it-dep-cda,...)
- **shibboleth_separator** = ";": The separator used to seperate groups in the *$_SERVER\[group\]* and *shibboleth_superuser_groups* strings.
- **shibboleth_superuser_groups** = "superusers": User groups that are granted super user access to Matomo; checked on every user authentication.

#### 2. API Request Authentication via Token
**Important:** In order to authenticate any API request via token, the target virtual host must not be secured by Shibboleth, since otherwise the API request will never reach Matomo in this case but will be rejected by Shibboleth beforehand.

To provide the possibility for API requests from *inside* the OpenShift cluster two components have been introduced:

1. An Apache virtual host serving Matomo configured to listen to port **8081**
2. A [headless](https://docs.okd.io/latest/architecture/core_concepts/pods_and_services.html#headless-services) OpenShift service *matomo-api* that makes the Matomo pod available *under matomo-api.NAMESPACE.svc:8081* inside the OpenShift cluster.

To provide the possibility for API requests from *outside* the Openshift cluster, a root for the *matomo-api* service has been created.

To [authenticate API requests via token](https://developer.matomo.org/api-reference/reporting-api#authenticate-to-the-api-via-token_auth-parameter), the token send with the request has to belong to an existing Matomo user with sufficient (super user) access permissions.

### Environments

For each environment, you need to create a seperate values.yaml file which will overwrite some fields in the default values configuration. It is necessary to include the specified fields in the example. The important information here is the database information and the exposed routes of the specific instance of the application.

### GroupPermissions

Added some extra functions as a gateway to add some egroup functionality with this plugin. The most important added function here is syncEGroup which is used by a cronjob to synchronize members of each registered egroup, and change the specific user permission in Matomo. This plugin will be used by the Operators in order to give permissions to egroups and its members.

### GitLab CI/CD Pipeline

Build and deployment are handled by the GitLab CI/CD Pipeline as specified in *.gitlab-ci.yaml*. This will change in the future to better handle multiple instances.

### Deploy a new instance

To create a new Matomo instance the following steps need to be done:

1. Create a new Paas Application
2. Request a new DB instance
3. In the git repository:
    * Create a new values.yaml file under your environment.
    * Store the DB credentials as secrets per environment.
    * Store openshift’s Service Account token as a secret so that the gitlab-ci can use it.
    * Modify the gitlab-ci.yaml:
        * Specify your environment variables
        * Add a new step in each stage regarding your instance.
4. Deploy the application and follow the installation procedure.

### Installation
To install Matomo from scratch (mainly in order to create a new database) one first has to make sure, that the file *config.ini.php* inside Matomo's config directory */opt/matomo/config* does not exist (e.g. delete the file via remote shell or OpenShift Web Interface terminal, etc.). Calling the Matomo service *NAMESPACE.web.cern.ch* will redirect to the installation screen where one can simply follow the [installation guide](https://matomo.org/docs/installation/). If, for any reason, there exists a database which is no longer of any use, one can delete it using any MySQL client (e.g. phpMyAdmin,..) manually. Finally, the configuration file *config.ini.php* should be included in the proper directory to ensure the expected behaviour.

After the installation is complete, connect to the DB, and save the superuser's token as a secret along with your DB credentials as it is needed for syncing the egroups.

### Deploy a new instance

To create a new Matomo instance the following steps need to be done:

1. Create a new Paas Application
2. Request a new DB instance
3. In the git repository:
    * Create a new values.yaml file under your environment.
    * Store the DB credentials as secrets per environment.
    * Store openshift’s Service Account token as a secret so that the gitlab-ci can use it.
    * Modify the gitlab-ci.yaml:
        * Specify your environment variables
        * Add a new step in each stage regarding your instance.
4. Deploy the application and follow the installation procedure.

### Installation
To install Matomo from scratch (mainly in order to create a new database) one first has to make sure, that the file *config.ini.php* inside Matomo's config directory */opt/matomo/config* does not exist (e.g. delete the file via remote shell or OpenShift Web Interface terminal, etc.). Calling the Matomo service *NAMESPACE.web.cern.ch* will redirect to the installation screen where one can simply follow the [installation guide](https://matomo.org/docs/installation/). If, for any reason, there exists a database which is no longer of any use, one can delete it using any MySQL client (e.g. phpMyAdmin,..) manually. Finally, the configuration file *config.ini.php* should be included in the proper directory to ensure the expected behaviour.

After the installation is complete, connect to the DB, and save the superuser's token as a secret along with your DB credentials as it is needed for syncing the egroups.

### Updating Matomo

[Updating Matomo](https://matomo.org/docs/update/#the-manual-three-step-update) to a newer version can simply be done
 by altering the *MATOMO_VERSION* environment variable in the Dockerfile. Find the available 
 builds [here](https://builds.matomo.org/). After changing the version in the Dockerfile the project has to be re-build and re-deployed. Finally, Matomo has to be opened in a web browser in case structural changes to the database are necessary. Matomo will then prompt the superuser to do so. If after db schema changes the server's behavior is not as expected, redeploy the application.
 
 
### Secrets

All environment credentials and serviceaccounts are kept as secrets in Gitlab secrets. This includes the database secrets.

### Migration

Matomo Migration Procedure:

Two things need to be done:
    1) The migration of the database
    2) The intergration with the webservices portal

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1) The full migration consists of three steps and takes around 6 days to complete:
    a) Run mysqldump to copy the database from the existing db instance to the new db instance.
        The command is: mysqldump -h dbod-piwik.cern.ch -u piwik -P 5509 --single-transaction --quick piwikdb | mysql -h <<host>> -P <<port>> -u <<user>> -pPassword <<the new matomo db>>;
    b) Enable maintenance mode in Matomo and upgrade database. To this put the following code in the config file:
    [General]
    maintenance_mode = 1
    [Tracker]
    record_statistics = 0
    The upgrade process is done by Matomo itself. This changes the schema of the database so that it matches the new one. After the upgrade, remove the above lines from the config file.
    c) Run the archiving process manually the first time. For some reason, the automatic archiving process fails in some cases. It usually happens on websites with a lot of data.
        This problem seems to be solved if running the archive process manually each month.

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

2) At the same time we have to change the webservices portal so that new entries will be entered in Matomo instead of Piwik.

Right now the webservices portal works in the following way:

a) A user requests to use piwik for his website. This is the code that runs https://gitlab.cern.ch/webservices/WebAdminServices/blob/master/WebAdminServices/PiwikMgmt.asmx.cs
b) The above code, depending on the function, calls some piwik scripts that exist on the piwik-api server.
c) The scripts call the piwik api and execute the request

Since we are deploying the application to openshift, I am not sure if we can work in a similar way. Maybe we should make the code to directly call piwik api, ommiting the in between step?

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

When the above is completed, we can change the piwik URL that the tracking code uses (piwik.web.cern.ch) to point to the new API url that is exposed for API requests
which is (api-test-matomo.apptest.cern.ch) for dev and (api-matomo.app.cern.ch) for prod.