import subprocess
import logging

from os.path import getmtime

try:
    last_matomo = getmtime('/var/run/last_matomo_update')
    matomo_timestamp = getmtime('/etc/httpd/conf.d/matomo_httpd.conf')

    if matomo_timestamp > last_matomo:
        cfg_check = subprocess.call(['apachectl', 'configtest'])
        if cfg_check == 0:
            subprocess.call(['kill', '-USR1', '1'])

            subprocess.call(['touch', '/var/run/last_matomo_update'])

except Exception as e:
    logging.error("Not able to reload Apache config: FAILED" + repr(e))
    print(repr(e))
