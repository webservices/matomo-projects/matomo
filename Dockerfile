FROM cern/cc7-base

ENV MATOMO_VERSION=3.12.0 \
    PHP=php71 \
    PATH=$PATH:/opt/rh/rh-php71/root/usr/bin

RUN yum -y update && \
    yum install -y gettext && \
    yum --setopt=tsflags=nodocs -y install deltarpm \
                                           rh-$PHP-php \
                                           rh-$PHP-php-curl \
                                           rh-$PHP-php-gd \
                                           rh-$PHP-php-cli \
                                           rh-$PHP-php-mysqlnd \
                                           rh-$PHP-php-mbstring \
                                           rh-$PHP-php-xml \
                                           rh-$PHP-php-ldap \
                                           httpd24-httpd \
                                           cronolog \
                                           shibboleth \
                                           log4shib \
                                           xmltooling-schemas \
                                           opensaml-schemas \
                                           libcurl-openssl && \
    rm -rf /var/cache/yum/* && \
    yum clean all && \
    curl -L -o /opt/matomo.tar.gz https://builds.matomo.org/matomo-$MATOMO_VERSION.tar.gz && \
    tar -C /opt -xf /opt/matomo.tar.gz && \
    rm /opt/matomo.tar.gz

ENV LD_PRELOAD=/opt/shibboleth/lib64/libcurl.so.4

COPY LoginCERN /opt/matomo/plugins/LoginCERN
COPY GroupPermissions /opt/matomo/plugins/GroupPermissions
COPY conf_files/config.ini.php /opt/matomo/config/config.ini.php
COPY conf_files/*.sh conf_files/shibboleth2.xml conf_files/liveness.py /
COPY conf_files/httpd.conf /opt/rh/httpd24/root/etc/httpd/conf/
COPY conf_files/*_httpd.conf /opt/rh/httpd24/root/etc/httpd/conf.d/

RUN chown -R :apache /opt/matomo && \
    chmod a+w /opt/matomo/matomo.js /opt/matomo/piwik.js && \
    touch /var/run/last_matomo_update && mkdir /var/log/httpd/ && \
    chmod -R a+rw /opt/matomo/plugins /opt/matomo/tmp/ /opt/matomo/config/ && \
    chmod -R a+rw /opt/rh/httpd24/root/etc/httpd/conf.d/ /opt/rh/httpd24/root/etc/httpd/conf/ && \
    chmod -R a+rwx /var/log/httpd/ && \
    mkdir -p /usr/local/httpd/ && chmod a+rw /usr/local/httpd && \
    mkdir -p /var/run/shibboleth/ && chmod a+rw /var/run/shibboleth && \
    chmod a+rw /etc/shibboleth/ && touch /etc/shibboleth/shibboleth2.xml && chmod a+rw /etc/shibboleth/shibboleth2.xml && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd && \
    chown root:root /var/log/shibboleth && chmod -R a+rw /var/log/shibboleth && \
    ln -sf /dev/stdout /var/log/shibboleth/shibd.log && \
    chmod a+x /apache.sh /shibboleth.sh && \
    mkdir -p /var/log/php/ && chmod a+rw /var/log/php/ && \
    sed -i "s@memory_limit.*@memory_limit = 6G@g" /etc/opt/rh/rh-$PHP/php.ini && \
    sed -i "s@max_execution_time.*@max_execution_time = 3600@g" /etc/opt/rh/rh-$PHP/php.ini && \
    sed -i "s@;error_log.*@error_log = /var/log/php_errors.log@g" /etc/opt/rh/rh-$PHP/php.ini && \
    sed -i "s@mysqli.reconnect.*@mysqli.reconnect = On@g" /etc/opt/rh/rh-$PHP/php.ini && \
    curl -o /etc/shibboleth/ADFS-metadata.xml http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/ADFS-metadata.xml && \
    curl -o /etc/shibboleth/attribute-map.xml http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/attribute-map.xml && \
    curl -o /etc/shibboleth/wsignout.gif http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/wsignout.gif

RUN cd /opt/matomo/misc/ && \
    mkdir geotemp && cd geotemp && \
    curl -o GeoLite2-City.tar.gz http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz && \
    tar -xvzf GeoLite2-City.tar.gz --strip-components=1 && \
    mv *.mmdb ../ && cd ../ && \
    rm -rf geotemp

ENTRYPOINT ["/apache.sh"]

EXPOSE 8080