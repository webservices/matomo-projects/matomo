<?php

/**
 * Part of the Piwik Login Shibboleth Plug-in.
 */

namespace Piwik\Plugins\LoginCERN;

use Piwik\Config as PiwikConfig;
use Piwik\Container\StaticContainer;

/**
 * The MIT License (MIT)
 * Copyright © 2014-2016 Pouyan Azari, http://go.uniwue.de/azari <pouyan.azari@uni-wuerburg.de>
 * Copyright © 2014-2016 University of Wuerzburg, http://go.uniwue.de/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * The Config instance of the plug-in.
 *
 * It manages the whole configuration aspects of the plug-in.
 * Any changes to configuration parameters should be changed here. The same values will also be used in the templates
 * so always check that the parameters here match the template.
 *
 * @author Pouyan Azari <pouyan.azari@uni-wuerzburg.de>
 * @license MIT
 * @copyright 2014-2016 University of Wuerzburg
 * @copyright 2014-2016 Pouyan Azari
 */
class Config
{
    /**
     * Default configuration for this plugin.
     */
    public static $defaultConfig = array(
        'logout_url' => '',
        'shibboleth_user_login' => '',
        'shibboleth_user_alias' => '',
        'shibboleth_user_email' => '',
        'shibboleth_separator' => ';',
        'shibboleth_group' => '',
        'shibboleth_superuser_groups' => ''
    );

    /**
     * Returns an INI option value that is stored in the `[LoginCERN]` config section.
     *
     *
     * @param string $optionName name of the given option
     *
     * @return mixed
     */
    public static function getConfigOption($optionName)
    {
        return self::getConfigOptionFrom(PiwikConfig::getInstance()->LoginCERN, $optionName);
    }

    /**
     * Returns the configuration options from the form.
     *
     * @param mix $config Option to be set.
     * @param string $optionName The name of option.
     */
    public static function getConfigOptionFrom($config, $optionName)
    {
        if (isset($config[$optionName])) {
            return $config[$optionName];
        }

        return self::getDefaultConfigOptionValue($optionName);
    }

    /**
     * Returns the default value of the given option.
     *
     * @param string $optionName
     *
     * @return mix
     */
    public static function getDefaultConfigOptionValue($optionName)
    {
        return @self::$defaultConfig[$optionName];
    }

    /**
     * Returns the logout url.
     *
     * @return string
     */
    public static function getLogoutUrl()
    {
        return self::getConfigOption('logout_url');
    }


    /**
     * Returns the Shibboleth key for the user login.
     *
     * @return string
     */
    public static function getShibbolethUserLogin()
    {
        return self::getConfigOption('shibboleth_user_login');
    }

    /**
     * Returns the Shibboleth key for the user alias.
     *
     * @return string
     */
    public static function getShibbolethUserAlias()
    {
        return self::getConfigOption('shibboleth_user_alias');
    }

    /**
     * Returns the Shibboleth key for the user email.
     *
     * @return string
     */
    public static function getShibbolethUserEmail()
    {
        return self::getConfigOption('shibboleth_user_email');
    }

    /**
     * Returns the Shibboleth SuperUser group key.
     *
     * @return string
     */
    public static function getShibbolethSuperUserGroups()
    {
        return self::getConfigOption('shibboleth_superuser_groups');
    }

    /**
     * Returns the Shibboleth Group key.
     *
     * @return string
     */
    public static function getShibbolethGroup()
    {
        return self::getConfigOption('shibboleth_group');
    }

    /**
     * Returns the separator for the Shibboleth results.
     *
     * @return string
     */
    public static function getShibbolethSeparator()
    {
        return self::getConfigOption('shibboleth_separator');
    }

    /**
     * Returns the plugins options with values from the default
     * for the values not set.
     *
     * @return array
     */
    public static function getPluginOptionValuesWithDefaults()
    {
        $result = self::$defaultConfig;
        foreach ($result as $name => $ignore) {
            $actualValue = self::getConfigOption($name);
            // special check for useKerberos which can be a string
            if ($name == 'use_webserver_auth' && $actualValue === 'false') {
                $actualValue = 0;
            }
            if (isset($actualValue)) {
                $result[$name] = $actualValue;
            }
        }

        return $result;
    }

    /**
     * Save the plugin options.
     *
     * @param $config
     */
    public static function savePluginOptions($config)
    {
        $logger = StaticContainer::getContainer()->get('Psr\Log\LoggerInterface');
        $loginCERN = PiwikConfig::getInstance()->LoginCERN;
        foreach (self::$defaultConfig as $name => $value) {
            if (isset($config[$name])) {
                $loginCERN[$name] = $config[$name];
            }
        }
        PiwikConfig::getInstance()->LoginCERN = $loginCERN;
        PiwikConfig::getInstance()->forceSave();
        $logger->info("[LoginCERN] Plugin Settings Changed");
    }
}
