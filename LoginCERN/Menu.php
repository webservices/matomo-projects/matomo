<?php

/**
 * Part of the Piwik Login Shibboleth Plug-in.
 */

namespace Piwik\Plugins\LoginCERN;

use Piwik\Menu\MenuAdmin;
use Piwik\Piwik;
use Piwik\Version;

/**
 * The MIT License (MIT)
 * Copyright © 2014-2016 Pouyan Azari, http://go.uniwue.de/azari <pouyan.azari@uni-wuerburg.de>
 * Copyright © 2014-2016 University of Wuerzburg, http://go.uniwue.de/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * The Menu configuration of the Plug-in.
 *
 * Here the menu settings of the plug-in can be set. At this moment the plug-in settings will reside in Settings.
 * If there are more menu related functionalities are needed, they can be added here.
 *
 * @author Pouyan Azari <pouyan.azari@uni-wuerzburg.de>
 * @license MIT
 * @copyright 2014-2016 University of Wuerzburg
 * @copyright 2014-2016 Pouyan Azari
 */
class Menu extends \Piwik\Plugin\Menu
{
    /**
     * Configures the menu inherited from Menu.
     *
     * @param Menu $menu The global Menu object.
     */
    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Piwik::hasUserSuperUserAccess()) {
            if (Version::VERSION >= 3) {
                $menu->addSystemItem('Login CERN', $this->urlForAction('admin'), $order = 30);
            } else {
                $menu->addSettingsItem('Login CERN', $this->urlForAction('admin'), $order = 30);
            }
        }
    }
}
