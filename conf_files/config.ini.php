; <?php exit; ?> DO NOT REMOVE THIS LINE
; If you want to change some of these default values, the best practise is to override
; them in your configuration file in config/config.ini.php.
; For example if you want to override action_title_category_delimiter,
; edit config/config.ini.php and add the following:

[General]
; If set to 1, Matomo will automatically redirect all http:// requests to https://
; If SSL / https is not correctly configured on the server, this will break Matomo
; If you set this to 1, and your SSL configuration breaks later on, you can always edit this back to 0
; it is recommended for security reasons to always use Matomo over https
;force_ssl = 1 Access from outside the openshift cluster is protected by Shibboleth
; Uncomment line below if you use a standard proxy
proxy_client_headers[] = HTTP_X_FORWARDED_FOR
proxy_host_headers[] = HTTP_X_FORWARDED_HOST
assume_secure_protocol = 1

; maximum number of rows for any of the Referers tables (keywords, search engines, campaigns, etc.), and Custom variables names
datatable_archiving_maximum_rows_referrers = 5000
; maximum number of rows for any of the Referers subtable (search engines by keyword, keyword by campaign, etc.), and Custom variables values
datatable_archiving_maximum_rows_subtable_referrers = 5000
; maximum number of rows for any of the Actions tables (pages, downloads, outlinks)
datatable_archiving_maximum_rows_actions = 5000
; maximum number of rows for pages in categories (sub pages, when clicking on the + for a page category)
datatable_archiving_maximum_rows_subtable_actions = 5000
; maximum number of rows for all individual Custom Dimensions reports, and for Custom Variables names report
datatable_archiving_maximum_rows_custom_variables = 5000
; maximum number of rows for the Custom Dimensions subtables (list of all Page URLs per dimension value), and for Custom Variables values reports
datatable_archiving_maximum_rows_subtable_custom_variables = 5000

; Add the trusted host that users can access the matomo UI. Any unprotected API urls should NOT be included here.
trusted_hosts[] = ${HOSTNAME_FQDN}

; Minimum advised memory limit in Mb in php.ini file (see memory_limit value). Set to "-1" to always use the configured memory_limit value in php.ini file. Same for archiving.
minimum_memory_limit = -1
minimum_memory_limit_when_archiving = -1

[database]
host = ${DB_CONFIG_HOST}
port = ${DB_CONFIG_PORT}
username = ${DB_CONFIG_USERNAME}
password = ${DB_CONFIG_PASSWORD}
dbname = ${DB_CONFIG_NAME}
tables_prefix = ${DB_CONFIG_PREFIX}
adapter = PDO\MYSQL
type = InnoDB
schema = Mysql

; if charset is set to utf8, Matomo will ensure that it is storing its data using UTF8 charset.
; it will add a sql query SET at each page view.
; Matomo should work correctly without this setting but we recommend to have a charset set.
charset = utf8

[log]
log_writers[] = file
log_level = INFO

[mail]
; default Email @hostname, if current host can't be read from system variables
defaultHostnameIfEmpty = ${HOSTNAME_FQDN}
; smtp (using the configuration below) or empty (using built-in mail() function)
transport = smtp
; optional; defaults to 25 when security is none or tls; 465 for ssl
port = 25
; SMTP server address
host = cernmx.cern.ch
type = ; SMTP Auth type. By default: NONE. For example: LOGIN
username = ; SMTP username
password = ; SMTP password
encryption = ; SMTP transport-layer encryption, either 'ssl', 'tls', or empty (i.e., none).

[Plugins]
; list of plugins (in order they will be loaded) that are activated by default in the Matomo platform
Plugins[] = CorePluginsAdmin
Plugins[] = CoreAdminHome
Plugins[] = CoreHome
Plugins[] = WebsiteMeasurable
Plugins[] = IntranetMeasurable
Plugins[] = Diagnostics
Plugins[] = CoreVisualizations
Plugins[] = Proxy
Plugins[] = API
Plugins[] = ExamplePlugin
Plugins[] = Widgetize
Plugins[] = Transitions
Plugins[] = LanguagesManager
Plugins[] = Actions
Plugins[] = Dashboard
Plugins[] = MultiSites
Plugins[] = Referrers
Plugins[] = UserLanguage
Plugins[] = DevicesDetection
Plugins[] = Goals
Plugins[] = Ecommerce
Plugins[] = SEO
Plugins[] = Events
Plugins[] = UserCountry
Plugins[] = GeoIp2
Plugins[] = VisitsSummary
Plugins[] = VisitFrequency
Plugins[] = VisitTime
Plugins[] = VisitorInterest
Plugins[] = ExampleAPI
Plugins[] = RssWidget
Plugins[] = Feedback
Plugins[] = Monolog
Plugins[] = LoginCERN

Plugins[] = UsersManager
Plugins[] = SitesManager
Plugins[] = Installation
Plugins[] = CoreUpdater
Plugins[] = CoreConsole
Plugins[] = ScheduledReports
Plugins[] = UserCountryMap
Plugins[] = Live
Plugins[] = CustomVariables
Plugins[] = PrivacyManager
Plugins[] = ImageGraph
Plugins[] = Annotations
Plugins[] = MobileMessaging
Plugins[] = Overlay
Plugins[] = SegmentEditor
Plugins[] = Insights
Plugins[] = Morpheus
Plugins[] = Contents
Plugins[] = BulkTracking
Plugins[] = Resolution
Plugins[] = DevicePlugins
Plugins[] = Heartbeat
Plugins[] = Intl
Plugins[] = Marketplace
Plugins[] = ProfessionalServices
Plugins[] = UserId
Plugins[] = CustomPiwikJs
Plugins[] = GroupPermissions

[LoginCERN]
logout_url = "https://login.cern.ch/adfs/ls/?wa=wsignout1.0"
shibboleth_user_login = "ADFS_LOGIN"
shibboleth_user_alias = "ADFS_FULLNAME"
shibboleth_user_email = "ADFS_EMAIL"
shibboleth_group = "ADFS_GROUP"
shibboleth_separator = ";"
shibboleth_superuser_groups = ${AUTH_GROUPS}
