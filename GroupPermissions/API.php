<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Piwik\Plugins\GroupPermissions;

use Exception;
use Piwik\Access;
use Piwik\Access\RolesProvider;
use Piwik\Container\StaticContainer;
use Piwik\Piwik;
use Piwik\Site;
use Piwik\Tracker\Cache;
use Piwik\Plugins\UsersManager\API as UsersManagerAPI;

class API extends \Piwik\Plugin\API
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var Access\RolesProvider
     */
    private $roleProvider;
    
    private static $instance = null;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->roleProvider = StaticContainer::get(RolesProvider::class);
    }

    public static function getInstance()
    {
        try {
            $instance = StaticContainer::get('GroupPermissions_API');
            if (!($instance instanceof API)) {
                // Exception is caught below and corrected
                throw new Exception('GroupPermissions_API must inherit API');
            }
            self::$instance = $instance;
            
        } catch (Exception $e) {
            self::$instance = StaticContainer::get('Piwik\Plugins\GroupPermissions\API');
            StaticContainer::getContainer()->set('GroupPermissions_API', self::$instance);
        }

        return self::$instance;
    }

    public function getGroupAccessFromSite($idSite)
    {
        Piwik::checkUserHasAdminAccess($idSite);
        
        $groups = $this->model->getAllGroups();
        $groupAccess = $this->model->getPermissionsOfSite($idSite);
        
        $data = array();
        
        foreach ($groups as $group) {
            $data[$group['idgroup']] = array('name' => $group['name'], 'access' => 'noaccess');
        }
        
        foreach ($groupAccess as $ga) {
            $data[$ga['idgroup']]['access'] =  $ga['access'];
        }
        
        $return = array();
        foreach ($data as $group) {
            $return[$group['name']] = $group['access'];
        }
        
        return $return;
    }
    

    public function getAllGroups()
    {
        return $this->model->getAllGroups();
    }
    
    public function getMembersOfGroup($idGroup)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        return $this->model->getMembersOfGroup($idGroup);
    }
    
    public function addUserToGroup($idGroup, $login)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        $idGroup = $this->model->getGroupWithId($idGroup);
        if (empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesNotExist", $idGroup));
        }
        $idGroup = $idGroup['idgroup'];
        
        $usersManagerApi = UsersManagerAPI::getInstance();
        if (!$usersManagerApi->userExists($login)) {
            throw new Exception(Piwik::translate("UsersManager_ExceptionUserDoesNotExist", $login));
        }
        
        $this->model->removeUserFromGroup($idGroup, $login);
        
        return $this->model->addUserToGroup($idGroup, $login);
    }

    public function removeUserFromGroup($idGroup, $login)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        $idGroup = $this->model->getGroupWithId($idGroup);
        if (empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesNotExist", $idGroup));
        }
        $idGroup = $idGroup['idgroup'];
        
        $usersManagerApi = UsersManagerAPI::getInstance();
        if (!$usersManagerApi->userExists($login)) {
            throw new Exception(Piwik::translate("UsersManager_ExceptionUserDoesNotExist", $login));
        }
        
        return $this->model->removeUserFromGroup($idGroup, $login);
    }
    
    public function setGroupAccess($name, $access, $idSites)
    {
        if ($access != 'noaccess') {
            $this->checkAccessType($access);
        }
        
        $idGroup = $this->model->getGroupWithName($name);
        if (empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesNotExist", $name));
        }
        $idGroup = $idGroup['idgroup'];
        
        // in case idSites is all we grant access to all the websites on which the current connected user has an 'admin' access
        if ($idSites === 'all') {
            $idSites = \Piwik\Plugins\SitesManager\API::getInstance()->getSitesIdWithAdminAccess();
        } // in case the idSites is an integer we build an array
        else {
            $idSites = Site::getIdSitesFromIdSitesString($idSites);
        }

        if (empty($idSites)) {
            throw new Exception('Specify at least one website ID in &idSites=');
        }
        // it is possible to set user access on websites only for the websites admin
        // basically an admin can give the view or the admin access to any user for the websites he manages
        Piwik::checkUserHasAdminAccess($idSites);

        foreach ($idSites as $idSite) {
            $this->model->removePermission($idGroup, $idSite);
        }

        // if the access is noaccess then we don't save it as this is the default value
        // when no access are specified
        if ($access != 'noaccess') {
            foreach ($idSites as $idSite) {
                $this->model->createPermission($idGroup, $idSite, $access);
            }
        }

        // we reload the access list which doesn't yet take in consideration this new user access
        Access::getInstance()->reloadAccess();
        Cache::deleteTrackerCache();
    }
    
    private function checkAccessType($access)
    {
        $roles = $this->roleProvider->getAllRoleIds();
        
        if (!in_array($access, $roles, true)) {
            throw new Exception(Piwik::translate("UsersManager_ExceptionAccessValues", implode(", ", $roles), $access));
        }
    }
    
    public function createGroup($groupName)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        $idGroup = $this->model->getGroupWithName($groupName);
        if (!empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesExist", $idGroup));
        }
        
        return $this->model->createGroup($groupName);
    }

    public function renameGroup($idGroup, $newName)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        $idGroup = $this->model->getGroupWithId($idGroup);
        if (empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesNotExist", $idGroup));
        }
        $idGroup = $idGroup['idgroup'];
        
        return $this->model->renameGroup($idGroup, $newName);
    }

    public function deleteGroup($idGroup)
    {
        Piwik::checkUserHasSuperUserAccess();
        
        $idGroup = $this->model->getGroupWithId($idGroup);
        if (empty($idGroup['idgroup'])) {
            throw new Exception(Piwik::translate("GroupPermissions_ExceptionGroupDoesNotExist", $idGroup));
        }
        $idGroup = $idGroup['idgroup'];
        
        $this->model->removeAllPermissionsOfGroup($idGroup);
        $this->model->removeAllUsersOfGroup($idGroup);
        
        return $this->model->deleteGroup($idGroup);
    }

    #################################################################################################################
    # Customized CERN
    #################################################################################################################

    public function syncEGroup() {
        $groups = $this->getAllGroups();
        $eGroupsSync = array();
        foreach($groups as $group){
            $eGroupsSync[] = $group['name'];
        }
        foreach($eGroupsSync as $groupName){
            echo("Synching e-group $groupName\n");
            $idGroup = $this->model->getGroupWithName($groupName);
            $idGroup = $idGroup['idgroup'];
            echo("Content of idGroup: $idGroup\n");
            $groupMembers  = $this->getMembersOfGroup($idGroup);
            $groupUsers = array();
            foreach($groupMembers as $members){
                $groupUsers[] = $members['login'];
            }
            $egroupUsers = $this->getUsersFromEgroup($groupName);
            echo("Content of groupUsers: " . implode(";",$groupUsers) . "\n");
            echo("Content of egroupUsers: " . implode(";",$egroupUsers) . "\n");

            // Add users that exist in egroups but not in matomo groups
            $addUsers = array_diff($egroupUsers, $groupUsers);
            echo("Content of addUsers: " . implode(";",$addUsers) . "\n");
            foreach($addUsers as $loginName){
                $usersManagerApi = UsersManagerAPI::getInstance();
                if (!$usersManagerApi->userExists($loginName)){
                    $randomPassword = bin2hex(random_bytes(8));
                    UsersManagerAPI::getInstance()->addUser(strtoupper($loginName), $this->randomPassword(), strtoupper($loginName) . "@cern.ch", $loginName);
                }
                $this->addUserToGroup($idGroup, $loginName);
            }

            // Remove users from matomo groups that no longer exist in egroups
            $removeUsers = array_diff($groupUsers, $egroupUsers);
            echo("Content of removeUsers: " . implode(";",$removeUsers) . "\n");
            foreach($removeUsers as $loginName){
                $this->removeUserFromGroup($idGroup, $loginName);
            }
        }
    }

    private function getUsersFromEgroup($groupName) {
		$ds=ldap_connect("xldap.cern.ch");
		$members = array();
		if ($ds) { 
			// read-only access bind
			$r=ldap_bind($ds);
			
			//search for e-group in Workgroups organic unit
			$sr=ldap_search($ds, "OU=Workgroups,DC=cern,DC=ch", "CN=" . $groupName	);  
			
			//Number of entries retrieved: ldap_count_entries($ds, $sr)
			//get all entries:
			$info = ldap_get_entries($ds, $sr);
			
			for ($i=0; $i<$info["count"]; $i++) 
			{
				for ($j=0; $j<$info[$i]["member"]["count"]; $j++) //we iterate through the members of the group
				{
					$x = $info[$i]["member"][$j];
					$members[] = strtoupper($this->ParseUserNameFromCN($x));				
				}
			}			
		}
		return $members;
    }
    
    private function ParseUserNameFromCN($cn) {
		//regex to parse the login name from the AD path, example:
		// CN=manuelmd,OU=Users,OU=Organic Units,DC=cern,DC=ch
		// "^CN=(.*)?,(.*)
		
		preg_match("/^CN=(.*?),(.*)$/", $cn, $matches);
	
		if (count($matches)>=1)
			return $matches[1];	
    }
    
    private function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, strlen($alphabet)-1); //use strlen instead of count
			$pass[$i] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
    }
    
    public function getAdminGroupAccessInArray($idSite){
        $allGroups = $this->getGroupAccessFromSite($idSite);
        $result = array_keys($allGroups, "admin");
        return $result;
    }

    public function checkEgroupValidity($groupName){
        $eGroupUsers = $this->getUsersFromEgroup($groupName);
        if (sizeof($eGroupUsers) < 1) {
            return false;
        } else {
            return true;
        }
    }

    public function checkUserValidity($user){
        $ds=ldap_connect("xldap.cern.ch");
		if ($ds) { 
			// read-only access bind
			$r=ldap_bind($ds);
			
			//search for e-group in Workgroups organic unit
            $sr=ldap_search($ds, "ou=Users,ou=Organic Units,DC=cern,DC=ch", "CN=" . $user	);

            $info = ldap_get_entries($ds, $sr);

            if(!isset($info[0])){
                return false;
            }

            return true;
        }
    }

}

    #################################################################################################################
    # /Customized CERN
    #################################################################################################################