<?php

/**
 * Part of the Piwik Login Shibboleth Plug-in.
 */

namespace Piwik\Plugins\LoginCERN;

use Piwik\Common;
use Piwik\Container\StaticContainer;
use Piwik\Piwik;
use Piwik\Plugin\Manager as PluginManager;
use Piwik\Url;
use Piwik\View;
use Piwik\Plugins\Login\FormLogin;

/**
 * The MIT License (MIT)
 * Copyright © 2014-2016 Pouyan Azari, http://go.uniwue.de/azari <pouyan.azari@uni-wuerburg.de>
 * Copyright © 2014-2016 University of Wuerzburg, http://go.uniwue.de/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/*
 * @author Pouyan Azari <pouyan.azari@uni-wuerzburg.de>
 * @license MIT
 * @copyright 2014-2016 University of Wuerzburg
 * @copyright 2014-2016 Pouyan Azari
 */

class Controller extends \Piwik\Plugin\ControllerAdmin
{

    /**
     * @var LoginCERNAuth
     */
    protected $auth;

    /**
     * Constructor.
     *
     * @param LoginCERNAuth $auth
     * @param SessionInitializer $sessionInitializer
     */
    public function __construct($auth = null)
    {
        parent::__construct();

        if (empty($auth)) {
            $auth = StaticContainer::get('Piwik\Auth');
        }
        $this->auth = $auth;
    }

    /**
     * Default action
     *
     * @return string
     */
    public function index()
    {
        return $this->login();
    }

    public function login($messageNoAccess = null, $infoMessage = false)
    {
        $authResult = $this->auth->authenticate();

        if (!$authResult->wasAuthenticationSuccessful()) {
            // TODO: add custom message
            throw new Exception(Piwik::translate('Login_LoginPasswordNotCorrect'));
        }

        // get redirect url
        $urlToRedirect = Url::getCurrentUrlWithoutQueryString();

//        $currentUrl = 'index.php';
//        if (($idSite = Common::getRequestVar('idSite', false, 'int')) !== false) {
//            $currentUrl .= '?idSite=' . $idSite;
//        }
//
//        $urlToRedirect = Common::getRequestVar('url', $currentUrl, 'string');
//        $urlToRedirect = Common::unsanitizeInputValue($urlToRedirect);


        Url::redirectToUrl($urlToRedirect);
    }


    /**
     * The Admin page for the Login Shibboleth Plugin.
     *
     * @return string
     */
    public function admin()
    {
        Piwik::checkUserHasSuperUserAccess();
        $view = new View('@LoginCERN/index');
        self::setBasicVariablesAdminView($view);

        $view->servers = array();
        $this->setBasicVariablesView($view);
        $view->shibbolethConfig = Config::getPluginOptionValuesWithDefaults();
        $view->isLoginControllerActivated = PluginManager::getInstance()->isPluginActivated('Login');

        return $view->render();
    }

    /**
     * Error message shown when an AJAX request has no access
     *
     * @param string $errorMessage
     * @return string
     */
    public function ajaxNoAccess($errorMessage)
    {
        return sprintf(
            '<div class="alert alert-danger">
                <p><strong>%s:</strong> %s</p>
                <p><a href="%s">%s</a></p>
            </div>',
            Piwik::translate('General_Error'),
            htmlentities($errorMessage, Common::HTML_ENCODING_QUOTE_STYLE, 'UTF-8', $doubleEncode = false),
            'index.php?module=' . Piwik::getLoginPluginName(),

            // TODO: add custom message
            Piwik::translate('Login_LogIn')
        );
    }

    /**
     * Logout current user
     *
     * @return void
     */
    public function logout()
    {
        $logoutUrl = Config::getLogoutUrl();
        if (empty($logoutUrl)) {
            Piwik::redirectToModule('CoreHome');
        } else {
            Url::redirectToUrl($logoutUrl);
        }
    }

}
