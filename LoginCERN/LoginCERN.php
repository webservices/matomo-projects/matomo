<?php

/**
 * Part of the Piwik LoginCERN PLugin.
 */

namespace Piwik\Plugins\LoginCERN;

use Exception;
use Piwik\Common;
use Piwik\Container\StaticContainer;
use Piwik\FrontController;
use Piwik\Piwik;
use Piwik\Plugin\Manager;
use Psr\Log\LoggerInterface;


/**
 *
 */
class LoginCERN extends \Piwik\Plugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Initiator.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logger = StaticContainer::get('Psr\Log\LoggerInterface');
    }

    /**
     * Register the hooks to this Plugin.
     * @see Piwik\Plugin::registerEvents
     */
    public function registerEvents()
    {
        $hooks = array(
            'Request.initAuthenticationObject' => 'initAuthenticationObject',
            'User.isNotAuthorized' => 'noAccess',
            'API.Request.authenticate' => 'ApiRequestAuthenticate',
            'AssetManager.getJavaScriptFiles' => 'getJsFiles',
            'AssetManager.getStylesheetFiles' => 'getStylesheetFiles'
        );
        return $hooks;
    }

    /**
     * Adds the JavaScript files that the plug-in needs to the global list.
     *
     * @param array $jsFiles The array containing the JavaScript file paths
     */
    public function getJsFiles(&$jsFiles)
    {
        $jsFiles[] = 'plugins/LoginCERN/angularjs/admin/admin.controller.js';
    }

    /**
     * Adds the style sheets files that the plug-in needs to the global list.
     *
     * @param array $stylesheetFiles The array containing the style sheet file paths
     */
    public function getStylesheetFiles(&$stylesheetFiles)
    {
        $stylesheetFiles[] = 'plugins/LoginCERN/angularjs/admin/admin.controller.less';
    }

    /**
     * Deactivate default Login module, as both cannot be activated together.
     */
    public function activate()
    {
        $this->logger->debug("[LoginCERN]: Activate plugin");
        if (Manager::getInstance()->isPluginActivated('Login') == true) {
            Manager::getInstance()->deactivatePlugin('Login');
        }
    }

    /**
     * Activate default Login module, as one of them is needed to access Piwik.
     */
    public function deactivate()
    {
        $this->logger->debug("[LoginCERN]: Deactivate plugin");
        if (Manager::getInstance()->isPluginActivated('Login') == false) {
            Manager::getInstance()->activatePlugin('Login');
        }
    }

    /**
     * Redirects to Login form with error message.
     * Listens to User.isNotAuthorized hook.
     */
    public function noAccess(Exception $exception)
    {
        $frontController = FrontController::getInstance();

        if (Common::isXmlHttpRequest()) {
            echo $frontController->dispatch(Piwik::getLoginPluginName(), 'ajaxNoAccess', array($exception->getMessage()));
            return;
        }

        echo $frontController->dispatch(Piwik::getLoginPluginName(), 'login', array($exception->getMessage()));
    }

    /**
     * Initializes the authentication object.
     * Listens to Request.initAuthenticationObject hook.
     *
     * @param bool $activateCookieAuth If the authentication is cookie based.
     */
    public function initAuthenticationObject()
    {
        $this->logger->debug("[LoginCERN]: Initializing the authentication object");
        StaticContainer::getContainer()->set('Piwik\Auth', new Auth());
    }

    /**
     * Set login name and authentication token for API request.
     * Listens to API.Request.authenticate hook.
     */
    public function ApiRequestAuthenticate($tokenAuth)
    {
        $this->logger->debug("[LoginCERN]: API authentication request");

        $auth = StaticContainer::getContainer()->get('Piwik\Auth');
        $auth->setLogin(null);
        $auth->setTokenAuth($tokenAuth);
    }

}
