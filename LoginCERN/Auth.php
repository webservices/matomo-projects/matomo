<?php

namespace Piwik\Plugins\LoginCERN;

use Piwik\AuthResult;
use Piwik\Container\StaticContainer;
use Piwik\Date;
use Piwik\Plugins\UsersManager\API;
use Piwik\Plugins\UsersManager\Model;
use Psr\Log\LoggerInterface;


class Auth implements \Piwik\Auth
{

    /**
     * @var string
     */
    protected $login;
    /**
     * @var string
     */
    protected $token_auth;

    /**
     * @var Model
     */
    private $userModel;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Initiator.
     */
    public function __construct()
    {
        $this->userModel = new Model();
        $this->logger = StaticContainer::get('Psr\Log\LoggerInterface');
    }

    /**
     * Authentication module's name, e.g., "LoginCERN".
     *
     * @return string
     */
    public function getName()
    {
        return 'LoginCERN';
    }

    /**
     * Authenticates user with Shibboleth.
     *
     * @return AuthResult
     */
    public function authenticate()
    {
        $this->logger->debug("[LoginCERN]: Starting authentication");

        // !!! we assume that Shibboleth takes care of the authentication !!!-> we just use the shibb auth info and fetch the
        // user from the db (or create a new one if it doesn't exist)
        if (isset($_SERVER[Config::getShibbolethUserLogin()])) {

            $this->logger->debug("[LoginCERN]: Starting Shibboleth Authentication");
            $this->login = $_SERVER[Config::getShibbolethUserLogin()];
            $this->token_auth = null;
            return $this->authenticateWithShibbEnv($this->login);

        } elseif (is_null($this->login)) {

            $this->logger->debug("[LoginCERN]: Starting token authentication");
            return $this->authenticateWithToken($this->token_auth);

        } elseif (!empty($this->login)) {

            $this->logger->debug("[LoginCERN]: Starting hash token authentication");
            return $this->authenticateWithTokenOrHashToken($this->token_auth, $this->login);

        }

        // this statement should never be reached if Config::getShibbolethUserLogin() is correctly configured and shibboleth is working
        return new AuthResult(AuthResult::FAILURE, $this->login, null);
    }

    private function authenticateWithShibbEnv($login)
    {
        $login = strtoupper($login);
        $user = $this->userModel->getUser(($login));

        // if user does not exist in DB, we create a new user (bec. shibboleth takes care of the auth anyway..)
        // Attention! If shibboleth is not active, then this will potentially grant access to everyone!
        if (empty($user['login'])) {

            $this->logger->debug("[LoginCERN]: User " . $login . "does not exist! Creating new user!");

            if (!isset($_SERVER[Config::getShibbolethUserEmail()]) || !isset($_SERVER[Config::getShibbolethUserAlias()])) {

                $this->logger->debug("[LoginCERN]: No Shibboleth user email or alias in _SERVER!");
                return new AuthResult(AuthResult::FAILURE, $login, null);
            }

            $userEmail = $_SERVER[Config::getShibbolethUserEmail()];
            $userAlias = $_SERVER[Config::getShibbolethUserAlias()];

            // although we don't use a password in connection with shibboleth, generate a random one just to be sure
            $randomPassword = bin2hex(random_bytes(8));

            // generate authentication token
            $authToken = API::getInstance()->createTokenAuth($login);

            // add the new user to the db
            $this->logger->debug("[LoginCERN]: Adding new user " . $login);
            $this->userModel->addUser(
                $login,
                md5($randomPassword),
                $userEmail,
                $userAlias,
                $authToken,
                Date::now()->getDatetime());


            // fetch new user from db. if it doesn't exist, st went terribly wrong..
            $user = $this->userModel->getUser($login);
            if (empty($user['login'])) {

                $this->logger->debug("[LoginCERN]: Apparently user creation failed!");
                return new AuthResult(AuthResult::FAILURE, $login, null);
            }
        }

        // check for super user access and update user
        if (isset($_SERVER[Config::getShibbolethGroup()])) {

            $this->logger->debug("[LoginCERN]: Checking for super user access!");
            $superUserGroups = explode(Config::getShibbolethSeparator(), Config::getShibbolethSuperUserGroups());
            $shibbolethUserGroups = explode(Config::getShibbolethSeparator(), $_SERVER[Config::getShibbolethGroup()]);

            $groupIntersection = array_intersect($superUserGroups, $shibbolethUserGroups);
            $isSuperUser = count($groupIntersection) > 0 ? true : false;
            $this->userModel->setSuperUserAccess($user['login'], $isSuperUser);
            $this->logger->debug("[LoginCERN]: User " . $user['login'] . " super user access: " . $isSuperUser);
        }

        return $this->authenticationSuccess($user);
    }

    private function authenticateWithToken($token)
    {
        $user = $this->userModel->getUserByTokenAuth($token);

        if (!empty($user['login'])) {
            return $this->authenticationSuccess($user);
        }

        return new AuthResult(AuthResult::FAILURE, null, $token);
    }

    private function authenticateWithTokenOrHashToken($token, $login)
    {
        $user = $this->userModel->getUser($login);

        // authenticate either with the token or the "hash token"
        if ($user['token_auth'] === $token || (!empty($user['token_auth'] && getHashTokenAuth($login, $user['token_auth']) === $token))) {
            return $this->authenticationSuccess($user);
        }

        return new AuthResult(AuthResult::FAILURE, $login, $token);
    }

    private function authenticationSuccess(array $user)
    {
        $this->setTokenAuth($user['token_auth']);

        $isSuperUser = (int)$user['superuser_access'];
        $code = $isSuperUser ? AuthResult::SUCCESS_SUPERUSER_AUTH_CODE : AuthResult::SUCCESS;

        return new AuthResult($code, $user['login'], $user['token_auth']);
    }

    /**
     * Accessor to compute the hashed authentication token.
     *
     * @param string $login user login
     * @param string $token_auth authentication token
     * @return string hashed authentication token
     */
    private static function getHashTokenAuth($login, $token_auth)
    {
        return md5($login . $token_auth);
    }

    /**
     * Returns the login of the user being authenticated.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Accessor to set login name
     *
     * @param string $login user login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * Returns the secret used to calculate a user's token auth.
     *
     * @return string
     */
    public function getTokenAuthSecret()
    {
        return $this->hashedPassword;
    }

    /**
     * Accessor to set authentication token
     *
     * @param string $token_auth authentication token
     */
    public function setTokenAuth($token_auth)
    {
        $this->token_auth = $token_auth;
    }

    /**
     * Sets the password to authenticate with.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if (empty($password)) {
            $this->hashedPassword = null;
        } else {
            $this->hashedPassword = UsersManager::getPasswordHash($password);
        }
    }

    /**
     * Sets the password hash to use when authentication.
     *
     * @param string $passwordHash The password hash.
     */
    public function setPasswordHash($passwordHash)
    {
        if ($passwordHash === null) {
            $this->hashedPassword = null;
            return;
        }

        // check that the password hash is valid (sanity check)
        UsersManager::checkPasswordHash($passwordHash, Piwik::translate('Login_ExceptionPasswordMD5HashExpected'));

        $this->hashedPassword = $passwordHash;
    }

}
