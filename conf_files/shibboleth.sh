#!/bin/sh

echo "--> Running shibboleth container..."

if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
 fi
 
envsubst < /shibboleth2.xml > /etc/shibboleth/shibboleth2.xml

rm -f /run/shibboleth/shibd.sock

echo "--> Running Shibboleth..."
exec /usr/sbin/shibd -F -c /etc/shibboleth/shibboleth2.xml
