#!/bin/bash

echo "--> Running matomo container..."

if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi

# This env variable subtitution is needed for shibboleth to work.
envsubst < /shibboleth2.xml > /etc/shibboleth/shibboleth2.xml

echo "--> Running Apache..."
#exec httpd -DFOREGROUND
exec /opt/rh/httpd24/root/usr/sbin/httpd-scl-wrapper -DFOREGROUND